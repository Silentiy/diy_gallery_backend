from django.conf import settings

from gallery_api.models import Photo
from rest_framework import serializers


class PhotoListSerializer(serializers.ModelSerializer):
    file = serializers.SerializerMethodField()

    class Meta:
        model = Photo
        fields = "__all__"

        read_only_fields = [
            "user",
            "is_approved",
            "approval_date",
            "is_on_deletion",
            "on_deletion_since",
            "updated_file",
            "updated_upload_date",
        ]

    @staticmethod
    def get_file(photo_obj):
        try:
            return settings.BASE_DOMAIN + photo_obj.file.url
        except Exception:
            return None

    user = serializers.ReadOnlyField(source="user.username")
