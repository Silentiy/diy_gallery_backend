from django import forms
from service_objects.services import Service


class GalleryUserPutProcess(Service):
    user_id = forms.IntegerField()

    def process(self):
        raise NotImplementedError
