from decouple import config


AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
]

SITE_ID = 1

SOCIALACCOUNT_PROVIDERS = {
    "google": {
        "APP": {
            "client_id": config("GOOGLE_APP_ID"),
            "secret": config("GOOGLE_APP_SECRET"),
            "key": "",
        },
        "AUTH_PARAMS": {
            "access_type": "online",
        },
        "OAUTH_PKCE_ENABLED": True,
    },
    "vk": {
        "APP": {
            "client_id": config("VK_APP_ID"),
            "secret": config("VK_APP_SECRET"),
            "key": "",
        },
    },
}

ACCOUNT_EMAIL_REQUIRED = True
SOCIALACCOUNT_QUERY_EMAIL = True

REST_AUTH = {
    "USER_DETAILS_SERIALIZER": "gallery_api.serializers.user.UserSerializer",
}
