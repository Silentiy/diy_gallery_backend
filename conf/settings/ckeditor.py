CKEDITOR_BASEPATH = "/static/ckeditor/ckeditor/"
CKEDITOR_UPLOAD_PATH = "editor/"

CKEDITOR_CONFIGS = {
    "default": {
        "skin": "moono-lisa",
        "toolbar_FominArticles": [
            {
                "name": "document",
                "items": [
                    "Templates",
                    "Preview",
                    "Save",
                    "-",
                    "-",
                    "Undo",
                    "Redo",
                    "-",
                    "Print",
                ],
            },
            {
                "name": "clipboard",
                "items": [
                    "Cut",
                    "Copy",
                    "Paste",
                    "PasteText",
                    "PasteFromWord",
                ],
            },
            {"name": "editing", "items": ["Find", "Replace", "-", "SelectAll"]},
            "/",  # put this to force next toolbar on new line
            {"name": "styles", "items": ["Styles", "Format", "Font", "FontSize"]},
            "/",
            {
                "name": "basicstyles",
                "items": [
                    "Bold",
                    "Italic",
                    "Underline",
                    "Strike",
                    "Subscript",
                    "Superscript",
                    "-",
                    "RemoveFormat",
                ],
            },
            {"name": "colors", "items": ["-", "TextColor", "BGColor", "-"]},
            "/",
            {
                "name": "paragraph",
                "items": [
                    "NumberedList",
                    "BulletedList",
                    "-",
                    "Outdent",
                    "Indent",
                    "JustifyLeft",
                    "JustifyCenter",
                    "JustifyRight",
                    "JustifyBlock",
                    "-",
                    "BidiLtr",
                    "BidiRtl",
                ],
            },
            {
                "name": "insert",
                "items": [
                    "Image",
                    "Table",
                    "Youtube",
                    "VideoDetector",
                    "Embed",
                    "HorizontalRule",
                    "Smiley",
                    "Link",
                    "Unlink",
                    "SpecialChar",
                    "PageBreak",
                ],
            },
            {"name": "html5videoGroup", "items": ["Html5video"]},
            "/",
            {"name": "tools", "items": ["Maximize", "ShowBlocks", "Source"]},
            {"name": "about", "items": ["About"]},
        ],
        "toolbar": "FominArticles",  # put selected toolbar config here
        "height": 291,
        "width": 800,
        "tabSpaces": 4,
        "removePlugins": ["stylesheetparser", "iframe"],
        "allowedContent": True,
        "extraPlugins": ",".join(
            [
                "uploadimage",  # the upload image feature
                "youtube",
                "videodetector",
                "html5video",
                "div",
                "autolink",
                "autoembed",
                "embed",
                "embedsemantic",
                "autogrow",
                "widget",
                "lineutils",
                "clipboard",
                "dialog",
                "dialogui",
                "elementspath",
            ]
        ),
    }
}
