from split_settings.tools import include

include(
    "django.py",
    "auth.py",
    "ckeditor.py",
    "database.py",
    "restframework.py",
    "swagger.py",
    "logger.py",
)
