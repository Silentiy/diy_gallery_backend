from decouple import config
import dj_database_url


# Database
# https://docs.djangoproject.com/en/4.1/ref/settings/#databases
DATABASES = {
    "default": dj_database_url.config(
        default=config(
            "DATABASE_URL", default="postgres://localhost/local_db_name", cast=str
        )
    )
}
